package com.dosideas.repository;

import com.dosideas.domain.User;
import com.dosideas.security.CustomUserDetails;
import javax.transaction.Transactional;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class UserRepositoryTest {
    @Autowired
    private UserRepository userRepository;
   
    @Test
    @WithUserDetails("zim")
    public void findCurrentFromContext_userIsLoggedIn_returnsUser() {
        CustomUserDetails customUser = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        assertEquals(1, customUser.getId());
    }    
    
    @Test
    @WithUserDetails("zim")
    public void findCurrent_userIsLoggedIn_returnsUser() {
        User user = userRepository.findCurrent();
        assertNotNull(user);
        assertEquals("zim", user.getUsername());
        System.out.println("-------------------------------------");
        System.out.println(user.toString());
        System.out.println("-------------------------------------");
    }
}
