-- -----------------------------------------------------------------------------
-- Enables support for MySQL syntax
-- -----------------------------------------------------------------------------
-- SET DATABASE SQL SYNTAX MYS TRUE;

-- -----------------------------------------------------------------------------
-- Tables
-- -----------------------------------------------------------------------------

CREATE TABLE user (
    id BIGINT PRIMARY KEY IDENTITY,
    username VARCHAR(50) UNIQUE,
    password VARCHAR(256) NOT NULL,
    enabled BOOLEAN NOT NULL,
    token VARCHAR(256) UNIQUE
);

