package com.dosideas.security.token;

import com.dosideas.domain.User;
import com.dosideas.repository.UserRepository;
import java.io.IOException;
import java.util.Optional;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

/**
 * Tries to log in an user using a token. If the token is available in the
 * request, it is used as a valid Principal.
 * Read more: http://docs.spring.io/spring-security/site/docs/current/reference/html/preauth.html
 */
public class TokenPreAuthenticationFilter extends AbstractPreAuthenticatedProcessingFilter {

    private final UserRepository userRepository;

    public TokenPreAuthenticationFilter(AuthenticationManager authenticationManager, UserRepository userRepository) {
        super.setAuthenticationManager(authenticationManager);
        this.userRepository = userRepository;
    }

    /**
     * Obtains a Principal from the request (usually in request header). If no
     * token is present, this method returns null and the other security filters
     * are executed.
     */
    @Override
    protected Object getPreAuthenticatedPrincipal(HttpServletRequest request) {
//        String token = request.getHeader("X-Token");
        String token = request.getParameter("token");
        Optional<User> user = userRepository.findByTokenAndTokenNotNull(token);
        return user.map(u -> u.getUsername()).orElse(null);
    }

    /**
     * This method MUST return some Credential for the Principal, if it was
     * available.
     */
    @Override
    protected Object getPreAuthenticatedCredentials(HttpServletRequest request) {
//        return request.getHeader("X-Token");
        return request.getParameter("token");
    }

    /**
     * This method is overriden for debug purposes only. Just skip it.
     */
    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException, ServletException {
        System.out.println(":( pre-authentication unsuccesful");
        failed.printStackTrace();
        super.unsuccessfulAuthentication(request, response, failed);
    }

    /**
     * This method is overriden for debug purposes only. Just skip it.
     */
    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, Authentication authResult) throws IOException, ServletException {
        System.out.println(":) pre-authentication succesful!");
        super.successfulAuthentication(request, response, authResult);
    }

}
