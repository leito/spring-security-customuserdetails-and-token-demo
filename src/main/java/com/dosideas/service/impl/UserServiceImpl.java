/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.dosideas.service.impl;

import com.dosideas.domain.User;
import com.dosideas.repository.UserRepository;
import com.dosideas.security.CustomUserDetails;
import com.dosideas.service.UserService;
import java.util.UUID;
import javax.transaction.Transactional;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    public User findCurrentUser() {
        CustomUserDetails  user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userRepository.findOne(user.getId());
    }
    
    @Override
    @PreAuthorize("isAuthenticated()")
    public String getNewToken() {
        User user = findCurrentUser();
        user.setToken(UUID.randomUUID().toString());
        return user.getToken();
    }
    
}
